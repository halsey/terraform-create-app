provider "google" {
  credentials = file("/home/rosouza/Downloads/fresh-sequence-246818-c3f8312a56f3.json")

 project = "fresh-sequence-246818"
 region  = "us-east1"
 zone    = "us-east1-b"
}

resource "google_container_cluster" "app" {
  name     = "app-cluster"
  location = "us-east1-b"

  initial_node_count = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "np" {
  name       = "my-node-pool"
  location   = "us-east1-b"
  cluster    = "${google_container_cluster.app.name}"
  node_count = 1
  
  autoscaling {
      min_node_count = 1
      max_node_count = 3
  }

  node_config {
    preemptible  = true
    machine_type = "n1-standard-1"
    disk_size_gb = 20

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}